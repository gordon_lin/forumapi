﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using NUnit.Framework;
using DBForumAPI.Models;
using DBForumAPI.Controllers;
using FakeItEasy;
using System.Data.Entity;
using System.Net.Http;
using System.Net;
using log4net;
using log4net.Config;

namespace DBForumAPI.NUnit
{
    /*
    [TestFixture]
    public class articleTest
    {
        Article a;
        Article b;
        IEnumerable<Article> allarticle;
        protected static readonly ILog log = LogManager.GetLogger(typeof(articleTest));

        [SetUp]
        public void init() { 
            
            
            //aricle
            allarticle = new[] { a, b };
            log4net.Config.XmlConfigurator.Configure();
            log.Debug("SetUp Initialized");
        }
        
        [Test]
        public void testGetAllArticles() {
            var db = new FakeForumDB{ Articles = { a,b} };
            var Acontroller = new ArticlesController(db);
            var result = Acontroller.GetAllArticles();
            Assert.AreEqual(allarticle, result);
        }
        
        [Test]
        public void testGetArticle() {
            var c=new Article { ArticleID = 1, Author = "jda", Content = "dadw" };
            var db = A.Fake<IForumDB>();
            A.CallTo(() => db.Articles.Find(1)).Returns(c);
            var Acontroller = new ArticlesController(db);
            var result = Acontroller.GetArticle(1);
            Assert.AreEqual(c, result);
        }

        [Test]
        [ExpectedException(typeof(HttpResponseException))]
        public void testGetArticleNull() {
            var db = A.Fake<IForumDB>();
            A.CallTo(() => db.Articles.Find(3)).Returns(null);
            var Acontroller = new ArticlesController(db);
            Acontroller.GetArticle(3);
        }

        [Test]
        public void testDeleteArticleOK() {
            var c=new Article{ArticleID=1};

            var db = A.Fake<IForumDB>();
            A.CallTo(() => db.Articles.Find(1)).Returns(c);

            var Acontroller = new ArticlesController(db);
            //Run
            var result =Acontroller.DeleteArticle(1);
            //Assertion
            A.CallTo(() => db.Articles.Remove(c)).MustHaveHappened();
            Assert.IsInstanceOf<HttpResponseMessage>(result);
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }

        [Test]
        public void testDeleteArticleNull() {
            var c = new Article { ArticleID = 1 };
            var db = A.Fake<IForumDB>();
            A.CallTo(() => db.Articles.Find(1)).Returns(null);
            var Acontroller = new ArticlesController(db);
            
            //Run
            var result = Acontroller.DeleteArticle(1);
            //Assertion
            A.CallTo(() => db.Articles.Remove(A<Article>.Ignored)).MustNotHaveHappened();
            Assert.IsInstanceOf<HttpResponseMessage>(result);
            Assert.AreEqual(HttpStatusCode.NotFound, result.StatusCode);
        }
        
        [Test]
        public void testPutArticle() {
            
            var db = A.Fake<IForumDB>();
            A.CallTo(() => db.SetModified(A<Article>.Ignored)).DoesNothing();
            A.CallTo(() => db.SaveChanges()).Returns(1);
            var Acontroller = new ArticlesController(db);
            var result = Acontroller.PutArticle(1, new Article {ArticleID=1,Author="a",Content="b",Topic="c" });
            Assert.IsInstanceOf<HttpResponseMessage>(result);
            Assert.AreEqual(HttpStatusCode.Created,result.StatusCode);
        }
        
        [Test]
        public void testPutArticleBadRequest() {
            var db = A.Fake<IForumDB>();
            var Acontroller = new ArticlesController(db);
            var result = Acontroller.PutArticle(1, new Article { });
            Assert.IsInstanceOf<HttpResponseMessage>(result);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }

        

        
    }
     */ 
}