﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DBForumAPI.App_Start;
using Thinktecture.IdentityModel.Http.Cors;
using Thinktecture.IdentityModel.Http.Cors.IIS;
using System.Data.Entity;
using DBForumAPI.Models;

namespace DBForumAPI
{
    // 注意: 如需啟用 IIS6 或 IIS7 傳統模式的說明，
    // 請造訪 http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            CorsConfig.RegisterCors(GlobalConfiguration.Configuration);
            //
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ForumDB>());
            //GlobalConfiguration.Configuration.Filters.Add(new ModelValidationFilter());
            //var config = GlobalConfiguration.Configuration;
            //config.Formatters.Insert(0, new JsonpMediaTypeFormatter());
        }
        
        void ConfigureCors(CorsConfiguration corsConfig)
        {
            corsConfig.ForResources("Articles").AllowAllOriginsAllMethodsAndAllRequestHeaders();
            corsConfig.ForResources("Replies").AllowAllOriginsAllMethodsAndAllRequestHeaders();
        }

        
    }
}