﻿using System.Web.Http;
using System.Web.Routing;

namespace DBForumAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Replies                        
            config.Routes.MapHttpRoute(
                name: "GetAllReply",
                routeTemplate: "api/Articles/Replies/",
                defaults: new { controller = "Replies", action = "GetAllReplies" },
                constraints: new { httpMethod=new HttpMethodConstraint("GET")}
            );
            config.Routes.MapHttpRoute(
                name: "Replies",
                routeTemplate: "api/Articles/{aid}/Replies/{id}",
                defaults: new { controller = "Replies",id=RouteParameter.Optional },
                constraints: new { aid = @"\d+",id=@"\d*" }
            );
            
            //Articles
            config.Routes.MapHttpRoute(
                name: "Articles",
                routeTemplate: "api/Articles/{id}",
                defaults: new { controller = "Articles", id = RouteParameter.Optional },
                constraints: new { id = @"\d*" }
            );
        }
    }
}
