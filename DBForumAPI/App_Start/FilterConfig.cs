﻿using System.Web;
using System.Web.Mvc;
using DBForumAPI.App_Start;
namespace DBForumAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }

}