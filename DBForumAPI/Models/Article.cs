﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Runtime.Serialization;
namespace DBForumAPI.Models
{
    public class Article
    {
        public long ArticleID { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Topic { get; set; }
        [Required]
        public string Content { get; set; }
        public DateTime Time { get; set; }
        [IgnoreDataMember] //ignore when serializing
        public ICollection<Reply> Replies { get; set; }
    }
}