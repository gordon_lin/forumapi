﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data;
using System.ComponentModel.DataAnnotations.Schema;
namespace DBForumAPI.Models
{
    public class ForumDB : DbContext
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Reply> Replies { get; set; }
        
        //Turning off EF Proxy makes [IgnoreDataMember] attribute working (in Article.cs/Reply.cs) when serializing.
        public ForumDB() { this.Configuration.ProxyCreationEnabled = false; }
        
        //Mapping to Database Columns
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Article>().Property(a => a.ArticleID).HasColumnName("Id");
            modelBuilder.Entity<Reply>().Property(r => r.ReplyID).HasColumnName("Id");
            modelBuilder.Entity<Reply>().Property(r => r.ArticleID).HasColumnName("ArticleId");
            base.OnModelCreating(modelBuilder);
        }
    }
}