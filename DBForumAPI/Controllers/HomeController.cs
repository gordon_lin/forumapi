﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DBForumAPI.Models;
using System.Net.Http;

namespace DBForumAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           return Redirect("/api/articles");
        }
        
    }
}
