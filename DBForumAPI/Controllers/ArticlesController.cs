﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Data;
using System.Data.Entity;
using DBForumAPI.Models;
using System.Globalization;
using System.Reflection;
using Common.Logging;
using DBForumAPI.App_Start;
namespace DBForumAPI.Controllers
{
    
    public class ArticlesController : ApiController
    {
        private ForumDB db = new ForumDB();
        protected static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        //[GET]   /api/articles        
        [HttpGet]
        public IEnumerable<Article> GetAllArticles()
        {
            return db.Articles.ToList();
        }   
        
        //[GET]   /api/articles/{id}
        [HttpGet]
        public Article GetArticle(int id)
        {
            Article item = db.Articles.Find(id);
            if (item == null)
            {
                log.ErrorFormat("Article #{0} Not Found",id);
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }        
        //[POST]  /api/articles
        [HttpPost]
        [ModelValidationFilter]
        public HttpResponseMessage PostArticle(Article item)
        {
            item.Time = DateTime.UtcNow;
            db.Articles.Add(item);
            db.SaveChanges();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Redirect);
            response.Headers.Location = new Uri(Url.Link("Articles", new { id = item.ArticleID }));
            return response;
            
        }
        //[PUT]   /api/articles/{id}
        [HttpPut]
        [ModelValidationFilter]
        public HttpResponseMessage PutArticle(int id,Article item)
        {
            item.ArticleID = id;
            item.Time = DateTime.UtcNow;
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);                
            return response;
        }
        //[DELETE] /api/articles/{id}
        [HttpDelete]
        public HttpResponseMessage DeleteArticle(int id)
        {
            Article item = db.Articles.Find(id);
            if (item == null)
            { log.ErrorFormat("Article #{0} Not Found",id); return new HttpResponseMessage(HttpStatusCode.NotFound); }
            db.Articles.Remove(item);
            db.SaveChanges();
            return new HttpResponseMessage(HttpStatusCode.NoContent);            
        } 
    }
}
