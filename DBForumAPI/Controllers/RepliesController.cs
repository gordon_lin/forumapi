﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using DBForumAPI.Models;
using System.Globalization;
using System.Reflection;
using Common.Logging;
using System.ComponentModel.DataAnnotations;
using DBForumAPI.App_Start;
namespace DBForumAPI.Controllers
{
    public class RepliesController : ApiController
    {
        protected readonly static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public ForumDB db = new ForumDB();

        //[GET]  /api/articles/replies
        [HttpGet]
        public IEnumerable<Reply> GetAllReplies()
        {
            return db.Replies.ToList();
        }
        //[GET]  /api/articles/{aid}/replies
        [HttpGet]
        public IEnumerable<Reply> GetArticleReplies(int aid)
        {
            //return db.Replies.Where(r => r.ArticleID == aid);
            return db.Articles.Include("Replies").Where(a=>a.ArticleID==aid).First().Replies.ToList();
            //return db.Articles.Find(aid).Replies.ToList();
        }
        //[GET]  /api/articles/{aid}/replies/id
        [HttpGet]
        public Reply GetReply(int aid,int id)
        {
            Reply item = db.Replies.Find(id);
            if (item == null)
            {
                log.ErrorFormat("Reply #{0} Not Found",id);
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }
        //[POST] /api/articles/{aid}/replies
        [HttpPost]
        [ModelValidationFilter]
        public HttpResponseMessage PostReply(int aid, Reply item)
        {
            item.Time = DateTime.UtcNow;
            item.ArticleID = aid;
            db.Replies.Add(item);
            db.SaveChanges();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Redirect);
            response.Headers.Location = new Uri(Url.Link("Replies", new { aid = aid, id = item.ReplyID }));
            return response;
        }
        //[PUT]  /api/articles/{aid}/replies/{id}
        [HttpPut]
        [ModelValidationFilter]
        public HttpResponseMessage PutReply(int aid,int id, Reply item)
        {
            item.ReplyID = id;
            item.ArticleID = aid;
            item.Time = DateTime.UtcNow;
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            return response;
        }
        //[DELETE]/api/articles/replies/{aid}/delete/{id}
        [HttpDelete]
        public HttpResponseMessage DeleteReply(int aid,int id)
        {
            Reply item = db.Replies.Find(id);
            if (item == null)
            { log.ErrorFormat("Reply #{0} Not Found",id); return new HttpResponseMessage(HttpStatusCode.NotFound); }
            db.Replies.Remove(item);
            db.SaveChanges();
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }

        
    }
}
