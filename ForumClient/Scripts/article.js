﻿function showallarticles() {
    
    $("#articles").empty();
    
    $.getJSON("http://localhost:2895/api/articles", function (data) {
        $.each(data, function (key, val) {
            showaddarticle(val);
        });
    });
}

$(document).ready(function () {
    showallarticles();
});

function deleteArticle(id) {
    $.ajax({
        type: 'DELETE',
        url: "http://localhost:2895/api/articles/" + id,
        success: function () {
            showdeletearticle(id);
        }
    });

}

function newArticle() {
    var a = encodeURI($("#author").val());
    var c = $("#content").val();
    var t = $("#topic").val();
    var str = { Author: a, Content: c, Topic: t };
    
    $.ajax({
        type: 'POST',
        url:'http://localhost:2895/api/articles/',
        data: str,
        success: function (data, status, xhr) {
            showaddarticle(data);
            $('#inputErrorMessage').hide();
            $("#author").val("");
            $("#topic").val("");
            $("#content").val("");
        },
        error: function (jqXhr) {
            var error = JSON.parse(jqXhr.responseText);
            var errorString='';
            for (var i = 0; i < error.length; i++)
            {
                errorString = errorString + ' ( ' + error[i].Value + ' ) ';
            }
            $('#inputErrorMessage').html(errorString);
            $('#inputErrorMessage').show();
        }
    });
    
}

function updateArticle(id) {
    var a = encodeURI($("#author").val());
    var c = $("#content").val();
    var t = $("#topic").val();
    var str = {Author: a, Content: c, Topic: t };
    $.ajax({
        type: 'PUT',
        url: 'http://localhost:2895/api/articles/' + id,
        data: str,
        success: function () {
            showupdatearticle(id);
            $('#inputErrorMessage').hide();
            $("#author").val("");
            $("#topic").val("");
            $("#content").val("");
        },
        error: function (jqXhr) {
            var error = JSON.parse(jqXhr.responseText);
            var errorString = '';
            for (var i = 0; i < error.length; i++) {
                errorString = errorString + ' ( ' + error[i].Value + ' ) ';
            }
            $('#inputErrorMessage').html(errorString);
            $('#inputErrorMessage').show();
        }
    });
}


function showaddarticle(val) {
    var time = new Date(val.Time);
    $('<div/>').prependTo($('#articles'));
    $('#articles div:first-child').attr('id', 'a' + val.ArticleID);
    $("<a/>", { text: "Delete" }).prependTo($('#a' + val.ArticleID));
    $('#a' + val.ArticleID+' a:first-child').attr('onclick', 'deleteArticle(' + val.ArticleID + ')');
    $("<a/>", { text: "Update" }).prependTo($('#a' + val.ArticleID));
    $('#a' + val.ArticleID + ' a:first-child').attr('onclick', 'updateArticle(' + val.ArticleID + ')');
    $("<a/>", { text: "ViewContent" }).prependTo($('#a' + val.ArticleID));
    $('#a' + val.ArticleID + ' a:first-child').attr('onclick', 'showcontentreply(' + val.ArticleID+')');
    var str = "Author : " + val.Author + " , Topic: " + val.Topic + " , Date: " + time;
    $('<li/>', { text: str }).prependTo($('#a' + val.ArticleID));
    $('#a' + val.ArticleID + ' li:first-child').attr('id', '#a' + val.ArticleID + 'content');
}

function showcontentreply(id) {
    currentrid = id;
    $('#articlesDiv').hide();
    $('#repliesDiv').show();
    showcontent(id);
    showallreplies(id);
}

function showdeletearticle(id) {
    $('#a' + id).remove();
}

function showupdatearticle(id) {
    $('#a' + id + ' li:first-child').remove();
    $.getJSON('http://localhost:2895/api/articles/' + id, function (val) {
        var time = new Date(val.Time);
        var str = "Author : " + val.Author + " , Topic: " + val.Topic + " , Date: " + time;
        $('<li/>', { text: str }).prependTo($('#a' + val.ArticleID));
        $('#a' + val.ArticleID + ' li:first-child').attr('id', '#a' + val.ArticleID + 'content');
    });
    
}


function testpost() {
    var str = { author:'cdadad' };
    $.ajax({
        type: 'POST',
        url: 'http://localhost:2895/api/articles/18',
        data: str
    });
}