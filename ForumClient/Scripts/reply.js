﻿function showcontent(rid) {
    $("#acontent").empty();
    $.getJSON("http://localhost:2895/api/articles/" + rid, function (data) {
        var str=data.Content;
        $('<p/>', { text: str }).appendTo($("#acontent"));
    });
}

function showallreplies(rid) {
    $("#replies").empty();
    $.getJSON("http://localhost:2895/api/articles/" + rid + "/replies/", function (data) {
        $.each(data, function (key, val) {
            showaddreplies(val);
        });
    });
}
$(document).ready(function () {
    $('#repliesDiv').hide();
    $('#inputErrorMessage').hide();
});

function newReply() {
    rid = currentrid;
    var c = $("#rcontent").val();
    var a = $("#rauthor").val();
    var str = { Author: a, Content: c };
    $.ajax({
        url: 'http://localhost:2895/api/articles/' + rid + '/replies',
        type:'POST',
        data: str,
        success: function (data) {
            showaddreplies(data);
            $("#rauthor").val("");
            $("#rcontent").val("");
            $('#inputErrorMessage').hide();
        },
        error: function (jqXhr) {
            var error = JSON.parse(jqXhr.responseText);
            var errorString = '';
            for (var i = 0; i < error.length; i++) {
                errorString = errorString + ' ( ' + error[i].Value + ' ) ';
            }
            $('#inputErrorMessage').html(errorString);
            $('#inputErrorMessage').show();
        }
    });
    
}

function deleteReply(rid,id) {
    $.ajax({
        type: 'DELETE',
        url: "http://localhost:2895/api/articles/" + rid + "/replies/" + id,
        success: function () { showdeletereply(id); }
    });
}

function updateReply(rid,id) {
    var c = $("#rcontent").val();
    var a = $("#rauthor").val();
    var str = {Author: a, Content: c};
    $.ajax({
        type: 'PUT',
        data: str,
        url: "http://localhost:2895/api/articles/" + rid + "/replies/" + id,
        success: function () {
            showupdatereply(id, rid);
            $("#rauthor").val("");
            $("#rcontent").val("");
            $('#inputErrorMessage').hide();
        },
        error: function (jqXhr) {
            var error = JSON.parse(jqXhr.responseText);
            var errorString = '';
            for (var i = 0; i < error.length; i++) {
                errorString = errorString + ' ( ' + error[i].Value + ' ) ';
            }
            $('#inputErrorMessage').html(errorString);
            $('#inputErrorMessage').show();
        }
    })
    
}
function showaddreplies(val) {
    var time = new Date(val.Time);
    $('<div/>').prependTo($('#replies'));
    $('#replies div:first-child').attr('id','r'+val.ReplyID);

    $("<a/>", { text: "Delete" }).prependTo($('#r' + val.ReplyID));
    $('#r' + val.ReplyID + ' a:first-child').attr('onclick', 'deleteReply(' + val.ArticleID + ',' + val.ReplyID + ')');
    $("<a/>", { text: "Update" }).prependTo($('#r' + val.ReplyID));
    $("#r" + val.ReplyID + " a:first-child").attr('onclick', 'updateReply(' + val.ArticleID + ',' + val.ReplyID + ')');
    var str = val.Author + " said : " + val.Content + "          at " + time;
    $('<li/>', { text: str }).prependTo($('#r' + val.ReplyID));
}
function showdeletereply(id) {
    $('#r' + id).remove();
}
function showupdatereply(id, rid) {
    $('#r' + id + ' li:first-child').remove();
    $.getJSON('http://localhost:2895/api/articles/' + rid + '/replies/' + id, function (val) {
        var time = new Date(val.Time);
        var str = val.Author + " said : " + val.Content + "          at " + time;
        $('<li/>', { text: str }).prependTo($('#r' + val.ReplyID));
    })
    
}

function goback() {
    $('#articlesDiv').show();
    $('#repliesDiv').hide();
    showallarticles();
}